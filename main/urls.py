from django.urls import path, include
from . import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns =[
    path('', views.home, name='home'),
    path('search', views.search, name='search'),
    path('category_list', views.category_list, name='category_list'),
    path('product_list', views.product_list, name='product_list'),
    path('category_product_list/<int:cat_id>', views.category_product_list, name='category_product_list'),
    path('product/<str:slug>/<int:id>', views.product_detail, name='product_detail'),
    path('filter-data', views.filter_data, name='filter_data'),
    path('pagination', views.pagination, name='pagination'),
    path('add-to-cart', views.add_to_cart, name='add_to_cart'),
    path('cart', views.cart_list, name='cart'),
    path('delete-from-cart', views.delete_cart_item, name='delete-from-cart'),
    path('update-cart', views.update_cart_item, name='update_cart'),
    path('accounts/profile', views.signup, name='signup'),
    path('login/', views.login, name='login'),
    path('checkout', views.checkout, name='checkout'),
    path('admin_dashboard/', views.admin_dashboard, name='admin_dashboard'),
    path('user_dashboard/', views.user_dashboard, name='user_dashboard'),
    path('orders/', views.orders, name='orders'),
    path('order_items/<int:id>', views.order_items, name='order_items'),
    path('cus_orders/', views.cus_orders, name='cus_orders'),
    path('cus_order_items/<int:id>', views.cus_order_items, name='cus_order_items'),

    path('addcategory/', views.add_category,name='addcategory'),
    path('updatecategory/<int:id>/', views.update_category,name='updatecategory'),
    path('deletecategory/<int:id>/', views.delete_category,name='deletecategory'),

    path('addsize/', views.add_size,name='addsize'),
    path('updatesize/<int:id>/', views.update_size,name='updatesize'),
    path('deletesize/<int:id>/', views.delete_size,name='deletesize'),

    path('addproduct/', views.add_product,name='addproduct'),
    path('updateproduct/<int:id>/', views.update_product,name='updateproduct'),
    path('deleteproduct/<int:id>/', views.delete_product,name='deleteproduct'),

    path('addproductp/', views.add_productp,name='addproductp'),
    path('update/<int:id>/', views.update_productp,name='updateproductp'),
    path('delete/<int:id>/', views.delete_productp,name='deleteproductp'),
   
    path('reports/', views.reports,name='reports'),

    path('adminlogin/', views.admin_login,name='adminlogin'),
    path('adminlogout/', views.admin_logout,name='adminlogout'),

    path('updateorder/<int:id>/', views.update_order,name='updateorder'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)