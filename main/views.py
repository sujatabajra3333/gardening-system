from urllib import response
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render,redirect, HttpResponseRedirect
from .models import Category, Product, ProductProperty, CartOrder,CartOrderItems, Size, User
from django.template.loader import render_to_string
from django.db.models.functions import ExtractMonth
from django.db.models import Count
from .forms import SignupForm, CategoryForm, SizeForm, ProductForm,ProductPropertyForm, LoginForm,CartOrderForm
from django.contrib import messages
from django.contrib.auth import authenticate,login,logout
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
import csv
import datetime
# Create your views here.
# Home Page
def home(request):
    data = Product.objects.filter(is_featured=True).order_by('-id')
    return render(request, "index.html",{'data':data})

# Category Page
def category_list(request):
    data = Category.objects.all().order_by('-id')
    return render(request,'category_list.html',{'data':data})

#Creating product list according to category
def product_list(request):
    total_data =Product.objects.count()
    data = Product.objects.all().order_by('-id')[:3]
    return render(request, 'product_list.html', 
    {
        'data':data,    
        'total_data':total_data,  
    }
    )

#Creating category product list according to category
def category_product_list(request,cat_id):
    category= Category.objects.get(id=cat_id)
    data = Product.objects.filter(category=category).order_by('-id')
    return render(request,'category_product_list.html',
    {
        'data':data,
    }
    )

#product detail 
def product_detail(request,slug,id):
    product = Product.objects.get(id=id)
    sizes=ProductProperty.objects.filter(product=product).values('size__id','size__title','price').distinct()
    return render(request,'product_detail.html',{'data':product,'sizes':sizes})

#Search
def search(request):
    q=request.GET['q']
    data = Product.objects.filter(title__icontains=q).order_by('-id')| \
                Product.objects.filter(detail__icontains=q).order_by('-id')
    return render(request,'search.html',{'data':data})

#Filterdata
def filter_data(request):
    categories = request.GET.getlist('category[]')
    sizes = request.GET.getlist('size[]')
    minPrice = request.GET['minPrice']
    maxPrice = request.GET['maxPrice']
    allProducts = Product.objects.all().order_by('-id').distinct()
    allProducts=allProducts.filter(productproperty__price__gte=minPrice)
    allProducts=allProducts.filter(productproperty__price__lte=maxPrice)

    if len(categories)>0:
        allProducts=allProducts.filter(category__id__in=categories).distinct()
    if len(sizes)>0:  
        allProducts=allProducts.filter(productproperty__size__id__in=sizes).distinct() 
    t = render_to_string('ajax/product_list.html',{'data':allProducts})
    return JsonResponse({'data': t})

# Loadmore Pagination
def pagination(request):
    offset=int(request.GET['offset'])
    limit=int(request.GET['limit'])
    data=Product.objects.all().order_by('-id')[offset:offset+limit]
    t=render_to_string('ajax/product_list.html',{'data':data})
    return JsonResponse({'data':t})

#Add to cart
def add_to_cart(request):
   # del request.session['cartdata']
    cart_p={}
    cart_p[str(request.GET['id'])]={
        'image':request.GET['image'],
        'title':request.GET['title'],
        'qty':request.GET['qty'],
        'price':request.GET['price'],
    }
    if 'cartdata' in request.session:
        if str(request.GET['id']) in request.session['cartdata']:
            cart_data= request.session['cartdata']
            cart_data[str(request.GET['id'])]['qty']=int(cart_p[str(request.GET['id'])]['qty'])            
            cart_data.update(cart_data)
            request.session['cartdata']=cart_data
        else:
            cart_data= request.session['cartdata']
            cart_data.update(cart_p)
            request.session['cartdata']=cart_data
    else:
        request.session['cartdata']=cart_p
    return JsonResponse({'data': request.session['cartdata'],'totalitems':len(request.session['cartdata'])})


# Cart List Page
def cart_list(request):
	total_amt=100
	if 'cartdata' in request.session:
		for p_id,item in request.session['cartdata'].items():
			total_amt+=int(item['qty'])*float(item['price'])
		return render(request, 'cart.html',{'cart_data':request.session['cartdata'],'totalitems':len(request.session['cartdata']),'total_amt':total_amt})
	else:
		return render(request, 'cart.html',{'cart_data':'','totalitems':0,'total_amt':total_amt})

#Delete Cart Item
def delete_cart_item(request):
    p_id = str(request.GET['id'])
    if 'cartdata' in request.session:
        if p_id in request.session['cartdata']:
            cart_data=request.session['cartdata']
            del request.session['cartdata'][p_id]
            request.session['cartdata'] = cart_data
    total_amt=100
    for p_id,item in request.session['cartdata'].items():
        total_amt+= int(item['qty'])*float(item['price'])
    t=render_to_string('ajax/cart_list.html',{'cart_data': request.session['cartdata'],'totalitems':len(request.session['cartdata']), 'total_amt':total_amt})
    return JsonResponse({'data':t,'totalitems':len(request.session['cartdata'])})

#Update Cart Item
def update_cart_item(request):
    p_id = str(request.GET['id'])
    p_qty= request.GET['qty']
    if 'cartdata' in request.session:
        if p_id in request.session['cartdata']:
            cart_data=request.session['cartdata']
            cart_data[str(request.GET['id'])]['qty']=p_qty
            request.session['cartdata'] = cart_data
    total_amt=100
    for p_id,item in request.session['cartdata'].items():
        total_amt+= int(item['qty'])*float(item['price'])
    t=render_to_string('ajax/cart_list.html',{'cart_data': request.session['cartdata'],'totalitems':len(request.session['cartdata']), 'total_amt':total_amt})
    return JsonResponse({'data':t,'totalitems':len(request.session['cartdata'])})

#Signup
def signup(request):
    if request.method=='POST':
        form=SignupForm(request.POST)
        if form.is_valid():
            form.save()
            username=form.cleaned_data.get('username')
            pwd=form.cleaned_data.get('password1')
            user= authenticate(username=username,password=pwd)
            login(request,user)
            return redirect('login')
    form = SignupForm
    return render(request,'registration/signup.html',{'form':form})

#Checkout
@login_required
def checkout(request):
    total_amt=100
    totalAmt=100
    if 'cartdata' in request.session:
        for p_id,item in request.session['cartdata'].items():
            totalAmt+=int(item['qty'])*float(item['price'])
        # Order
        order=CartOrder.objects.create(
                user=request.user,
                total_amt=totalAmt
            )
        # End
        for p_id,item in request.session['cartdata'].items():
            total_amt+=int(item['qty'])*float(item['price'])
            # OrderItems
            items=CartOrderItems.objects.create(
                order=order,
                invoice_no='INV-'+str(order.id),
                item=item['title'],
                image=item['image'],
                qty=item['qty'],
                price=item['price'],
                total=float(item['qty'])*float(item['price'])
                )
            # End
        # Process Payment
        return render(request, 'checkout.html',{'cart_data':request.session['cartdata'],'totalitems':len(request.session['cartdata']),'total_amt':total_amt})

#UserDashboard
def user_dashboard(request):
    orders=CartOrder.objects.all()
    return render(request,'user/dashboard.html',{'orders':orders})

#AdminDashboard
import calendar
@staff_member_required
def admin_dashboard(request):
    orders=CartOrder.objects.annotate(month=ExtractMonth('order_dt')).values('month').annotate(count=Count('id')).values('month','count')
    monthNumber=[]
    totalOrders=[]
    for d in orders:
        monthNumber.append(calendar.month_name[d['month']])
        totalOrders.append(d['count'])
    return render(request,'admin/admin_dashboard.html',{'monthNumber': monthNumber,'totalOrders':totalOrders})

#Orders
def orders(request):
    orders = CartOrder.objects.filter(user=request.user).order_by('-id')
    return render(request,'user/orders.html',{'orders':orders})

#orderitems
def order_items(request,id):
    order=CartOrder.objects.get(pk=id)
    orderitems = CartOrderItems.objects.filter(order=order).order_by('-id')
    return render(request,'user/order_items.html',{'orderitems':orderitems})

#Orders
def cus_orders(request):
    orders = CartOrder.objects.all().order_by('-id')
    return render(request,'admin/cus_orders.html',{'orders':orders})

def  update_order(request, id):
    if request.user.is_authenticated:
        if request.method == "POST":
            si = CartOrder.objects.get(pk=id)
            form = CartOrderForm(request.POST, instance=si)
            if form.is_valid():
                form.save()
        else:
            si = CartOrder.objects.get(pk=id)
        form = CartOrderForm(instance=si)
        return render(request, 'admin/update_order.html',{'form':form,'size':si})
    else:
        return HttpResponseRedirect('/login/')

#orderitems
def cus_order_items(request,id):
    order=CartOrder.objects.get(pk=id)
    orderitems = CartOrderItems.objects.filter(order=order).order_by('-id')
    return render(request,'admin/cus_order_items.html',{'orderitems':orderitems})

#Adding and listing category in admin dashboard
def add_category(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = CategoryForm(request.POST, request.FILES)
            if form.is_valid():
                title = form.cleaned_data['title']
                image = form.cleaned_data['image']
                cat = Category(title=title,image=image)
                cat.save()
                form = CategoryForm()
        else:
            form = CategoryForm
        cat = Category.objects.all().order_by('-id')
        return render(request, 'admin/add_category.html',{'form':form,'cat':cat})
    else:
        return HttpResponseRedirect('/login/')

#Updating category in admin dashboard
def update_category(request, id):
    if request.user.is_authenticated:
        if request.method == "POST":
            pi = Category.objects.get(pk=id)
            form = CategoryForm(request.POST,request.FILES, instance=pi)
            if form.is_valid():
                form.save()
        else:
            pi = Category.objects.get(pk=id)
        form = CategoryForm(instance=pi)
        return render(request, 'admin/update_category.html',{'form':form})
    else:
        return HttpResponseRedirect('/login/')

#Delete category in admin dashboard
def delete_category(request, id):
    if request.user.is_authenticated:
        if request.method=="POST":
            pi= Category.objects.get(pk=id)
            pi.delete()
        return HttpResponseRedirect('/addcategory/')
    else:
        return HttpResponseRedirect('/login/')


#Adding and listing category in admin dashboard
def add_size(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = SizeForm(request.POST)
            if form.is_valid():
                title = form.cleaned_data['title']
                size = Size(title=title)
                size.save()
                form = SizeForm()
        else:
            form = SizeForm
        size = Size.objects.all().order_by('-id')
        return render(request, 'admin/add_size.html',{'form':form,'size':size})
    else:
        return HttpResponseRedirect('/login/')

#Update size in admin dashboard
def  update_size(request, id):
    if request.user.is_authenticated:
        if request.method == "POST":
            si = Size.objects.get(pk=id)
            form = SizeForm(request.POST, instance=si)
            if form.is_valid():
                form.save()
        else:
            si = Size.objects.get(pk=id)
        form = SizeForm(instance=si)
        return render(request, 'admin/update_size.html',{'form':form,'size':si})
    else:
        return HttpResponseRedirect('/login/')

#Delete size in admin dashboard
def delete_size(request, id):
    if request.user.is_authenticated:
        if request.method=="POST":
            si= Size.objects.get(pk=id)
            si.delete()
        return HttpResponseRedirect('/addsize/')
    else:
        return HttpResponseRedirect('/login/')

#Creating product list according to category
def add_product(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = ProductForm(request.POST)
            if form.is_valid():
                form.save()
                form = ProductForm()
        else:
            form = ProductForm
        products = Product.objects.all().order_by('-id')
        return render(request, 'admin/add_product.html',{'form':form,'products':products})
    else:
        return HttpResponseRedirect('/login/')

#Update product in admin dashboard
def  update_product(request, id):
    if request.user.is_authenticated:
        if request.method == "POST":
            pro = Product.objects.get(pk=id)
            form = ProductForm(request.POST, instance=pro)
            if form.is_valid():
                form.save()
        else:
            pro = Product.objects.get(pk=id)
        form = ProductForm(instance=pro)
        return render(request, 'admin/update_product.html',{'form':form,'product':pro})
    else:
        return HttpResponseRedirect('/login/')

#Delete product in admin dashboard
def delete_product(request, id):
    if request.user.is_authenticated:
        if request.method=="POST":
            pro= Product.objects.get(pk=id)
            pro.delete()
        return HttpResponseRedirect('/addproduct/')
    else:
        return HttpResponseRedirect('/login/')

#Creating product property according to category
def add_productp(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = ProductPropertyForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
                form = ProductPropertyForm()
        else:
            form = ProductPropertyForm
        productp = ProductProperty.objects.all().order_by('-id')
        return render(request, 'admin/add_productp.html',{'form':form,'productp':productp})
    else:
        return HttpResponseRedirect('/login/')

#Update product property in admin dashboard
def  update_productp(request, id):
    if request.user.is_authenticated:
        if request.method == "POST":
            prop = ProductProperty.objects.get(pk=id)
            form = ProductPropertyForm(request.POST, request.FILES, instance=prop)
            if form.is_valid():
                form.save()
        else:
            prop = ProductProperty.objects.get(pk=id)
        form = ProductPropertyForm(instance=prop)
        return render(request, 'admin/update_productp.html',{'form':form,'productp':prop})
    else:
        return HttpResponseRedirect('/login/')

#Delete product property in admin dashboard
def delete_productp(request, id):
    if request.user.is_authenticated:
        if request.method=="POST":
            prop = ProductProperty.objects.get(pk=id)
            prop.delete()
        return HttpResponseRedirect('/addproductp/')
    else:
        return HttpResponseRedirect('/login/')

#Reports in the admin page
def reports(request):
    user_count = User.objects.all().count()
    category_count = Category.objects.all().count()
    product_count = Product.objects.all().count()
    orders=CartOrder.objects.annotate(month=ExtractMonth('order_dt')).values('month').annotate(count=Count('id')).values('month','count')
    monthNumber=[]
    totalOrders=[]
    for d in orders:
        monthNumber.append(calendar.month_name[d['month']])
        totalOrders.append(d['count'])    
    return render(request, 'admin/reports.html',{'user_count':user_count,'category_count':category_count,'product_count':product_count,'monthNumber': monthNumber,'totalOrders':totalOrders})

#admin logout
def admin_logout(request):
    logout(request)
    return HttpResponseRedirect('/adminlogin')

#admin login
def admin_login(request):
    if not request.user.is_authenticated:      
        if request.method == "POST":
            form = LoginForm(request=request, data=request.POST)
            if form.is_valid():
                uname = form.cleaned_data['username']
                pw = form.cleaned_data['password']
                user = authenticate(username = uname, password=pw)
                if user is not None:
                    login(request, user)
                    messages.success(request, 'Logged in succesfully!')
                    return HttpResponseRedirect('/admin_dashboard/')
        else:
            form = LoginForm()
        return render(request,'registration/adminlogin.html', {'form':form})
    else:
        return HttpResponseRedirect('/adminlogin/')    
        
