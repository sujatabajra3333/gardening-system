from django.contrib import admin
from .models import Category, Size, Product, ProductProperty,CartOrder, CartOrderItems
# Register your models here.

admin.site.register(Size)

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id','title','image')
admin.site.register(Category, CategoryAdmin)    

class ProductAdmin(admin.ModelAdmin):
    list_display=('id', 'title','category','status','is_featured')
    list_editable = ('status','is_featured',)
admin.site.register(Product, ProductAdmin)

class ProductPropertyAdmin(admin.ModelAdmin):
    list_display=('id','image_tag', 'product','price','size')
admin.site.register(ProductProperty, ProductPropertyAdmin)

class CartOrderAdmin(admin.ModelAdmin):
    list_display = ('user', 'total_amt', 'paid_status','order_dt','order_status')
    list_editable = ('paid_status','order_status')
admin.site.register(CartOrder, CartOrderAdmin)   

class CartOrderItemsAdmin(admin.ModelAdmin):
    list_display = ('invoice_no', 'item', 'image_tag','qty','price','total')
admin.site.register(CartOrderItems, CartOrderItemsAdmin)   

