$(document).ready(function(){
    $("#pagination").on('click',function(){
      var _currentProducts=$(".product-box").length ;
      var _limit=$(this).attr('data-limit');
      var _total=$(this).attr('data-total');
     // console.log(_currentProducts, _limit, _total);

     //Ajax

     $.ajax({
        url:'/pagination',
        data:{
            limit: _limit,
            offset: _currentProducts,
        },
        dataType:'json',
        beforeSend:function(){
            $("#pagination").attr('disabled',true);
            $(".load-more-icon").addClass('fa-spin');
        },
        success:function(res){
            // console.log(res);
            $("#filteredProducts").append(res.data);
            $("#pagination").attr('disabled',false);
            $(".load-more-icon").removeClass('fa-spin');

            var _totalShowing=$(".product-box").length;
            if(_totalShowing==_total){
                $('#pagination').remove();
            }
            }
        });
    });   
});

