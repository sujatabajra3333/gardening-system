$(document).ready(function(){
    // Show the price according to selected size
    $(".choose-size").first().addClass('active');
    $(".choose-size").on('click',function(){
        $(".choose-size").removeClass('active');
        $(this).addClass('active');

        var _price=$(this).attr('data-price');
        $(".product-price").text(_price);
    })

//Add to cart
    $(document).on('click', "#addToCartBtn",function(){
        var _vm = $(this);
        var _qty=$("#productQty").val();
        var _productId=$(".product-id").val();
        var _productImage=$(".product-image").val();
        var _productTitle=$(".product-title").val();
        var _productPrice=$(".product-price").text();
        //console.log(_qty,_productId,_productTitle);

        //ajax
        $.ajax({
			url:'/add-to-cart',
			data:{
                'id': _productId,
                'image': _productImage,
                'qty':_qty,
                'title':_productTitle,
                'price':_productPrice,
            },
			dataType:'json',
			beforeSend:function(){
				_vm.attr('disabled',true);
			},
			success:function(res){
				$(".cart-list").text(res.totalitems);
                _vm.attr('disabled',false);

			} 
		});

    });

    //Delete item from cart
    $(document).on('click','.delete-item',function(){
        var _pId=$(this).attr('data-item');
        var _vm=$(this);
        $.ajax({
            url:'/delete-from-cart',
            data:{
                'id': _pId,
            },
            dataType:'json',
            beforeSend:function(){
                _vm.attr('disabled',true);
            },
            success:function(res){
                $(".cart-list").text(res.totalitems);
                _vm.attr('disabled',false);
                $("#cartList").html(res.data);
            } 
        });
    });


    //Update item from cart
    $(document).on('click','.update-item',function(){
        var _pId=$(this).attr('data-item');
        var _pQty=$(".product-qty-"+_pId).val();
        var _vm=$(this);
        $.ajax({
            url:'/update-cart',
            data:{
                'id': _pId,
                'qty':_pQty
            },
            dataType:'json',
            beforeSend:function(){
                _vm.attr('disabled',true);
            },
            success:function(res){
               // $(".cart-list").text(res.totalitems);
                _vm.attr('disabled',false);
                $("#cartList").html(res.data);
            } 
        });
    });

});

