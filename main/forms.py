from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm,UsernameField  
from .models import Category, Size, Product, ProductProperty, CartOrder
from django.utils.translation import gettext, gettext_lazy as _
# from .models import Customer
class SignupForm(UserCreationForm):
    full_name=forms.CharField(max_length=50,required=True)
    mobile=forms.CharField(widget=forms.TextInput(attrs={'type':'number'}))
    address=forms.CharField(max_length=50,required=True)
    password1 = forms.CharField(label='Enter password', 
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirm password', 
                                widget=forms.PasswordInput)
    class Meta:
        model=User
        fields= ['full_name','mobile','address','username','password1','password2']
        help_texts = {
            "username":None,
            "password1":None,
            "password2":None,
            }

class LoginForm(AuthenticationForm):
    username = UsernameField(widget=forms.TextInput(attrs={'autofocus': True, 'class':'form-control'}))
    password = forms.CharField(label= _("Password"), strip=False, widget=forms.PasswordInput(attrs={'autocomplete': 'current-password', 'class':'form-control'}))

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['title','image']
        widgets = {
            'title' : forms.TextInput(attrs={'class':'form-control'}),
        }

class SizeForm(forms.ModelForm):
    class Meta:
        model = Size
        fields = ['title']
        widgets = {
            'title' : forms.TextInput(attrs={'class':'form-control'}),
        }

class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['title','slug','detail','desc','category','status','is_featured']
        widgets = {
            'title' : forms.TextInput(attrs={'class':'form-control'}),
            'slug' : forms.TextInput(attrs={'class':'form-control'}),
            'detail' : forms.TextInput(attrs={'class':'form-control'}),
            'desc' : forms.TextInput(attrs={'class':'form-control'}),
        }

    def __init__(self,*args,**kwargs):
        super(ProductForm,self).__init__(*args,**kwargs)
        self.fields['category'].empty_label = "Select"

class ProductPropertyForm(forms.ModelForm):
    class Meta:
        model = ProductProperty
        fields = ['product','size','price','image']

    def __init__(self,*args,**kwargs):
        super(ProductPropertyForm,self).__init__(*args,**kwargs)
        self.fields['product'].empty_label = "Select"
        self.fields['size'].empty_label = "Select"

class CartOrderForm(forms.ModelForm):
    class Meta:
        model = CartOrder
        fields = ['paid_status','order_status']
