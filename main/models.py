from django.db import models
from django.utils.html import mark_safe
from django.contrib.auth.models import User

# Create your models here.
#Category
class Category(models.Model):
    title = models.CharField(max_length=100)
    image = models.ImageField(null=True, blank= True, upload_to="cat_imgs/")

    class Meta:
        verbose_name_plural = "1. Categories"

    def __str__(self):
        return self.title

#Size    
class Size(models.Model):
    title = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = "2. Sizes"

    def __str__(self):
        return self.title

#Product Model
class Product(models.Model):
    title = models.CharField(max_length=200)
    slug = models.CharField(max_length=500)
    detail = models.TextField(null=True, blank=True)
    desc = models.TextField(null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    status=models.BooleanField(default=True)
    is_featured = models.BooleanField(default=False)
    
    class Meta:
        verbose_name_plural = "3. Products"

    def __str__(self):
        return self.title

#Product Attribute
class ProductProperty(models.Model):
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    size = models.ForeignKey(Size, on_delete=models.CASCADE)
    price = models.PositiveIntegerField(default=0)
    image = models.ImageField(upload_to="product_imgs/", null=True)

    class Meta:
        verbose_name_plural = "4. ProductProperty"

    def image_tag(self):
        return mark_safe('<img src="%s" width="50" height="50" />' % (self.image.url))

    def __str__(self):
        return self.product.title

#Order
status_choice=(
        ('process','In Process'),
        ('shipped','Shipped'),
        ('delivered','Delivered'),
    )
class CartOrder(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    total_amt=models.FloatField()
    paid_status=models.BooleanField(default=False)
    order_dt=models.DateTimeField(auto_now_add=True)
    order_status=models.CharField(choices=status_choice,default='process',max_length=150)

    class Meta:
        verbose_name_plural='5. Orders'

    def __str__(self):
            return self.user.username
# OrderItems 
class CartOrderItems(models.Model):
    order=models.ForeignKey(CartOrder,on_delete=models.CASCADE)
    invoice_no=models.CharField(max_length=150)
    item=models.CharField(max_length=150)
    image=models.CharField(max_length=200)
    qty=models.IntegerField()
    price=models.FloatField()
    total=models.FloatField()

    class Meta:
        verbose_name_plural='6. Order Items'

    def image_tag(self):
        return mark_safe('<img src="/media/%s" width="50" height="50" />' % (self.image))

