from .models import Product, ProductProperty
from django.db.models import Min, Max
def get_filters(request):
    cats = Product.objects.all().distinct().values('category__title','category__id')
    sizes = ProductProperty.objects.all().distinct().values('size__title','size__id')
    minmaxPrice=ProductProperty.objects.aggregate(Min('price'),Max('price'))
    data={
          'cats':cats, 
          'sizes':sizes,
          'minmaxPrice':minmaxPrice,
    }
    return data
