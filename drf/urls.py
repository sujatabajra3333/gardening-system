from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static

from rest_framework.routers import DefaultRouter
from drf.views import CategoryViewSet,SizeViewSet, ProductPropertyViewSet, ProductViewSet, ListUsers, CartOrderViewSet,CartOrderItemsViewSet
from drf.views import *

router = DefaultRouter()
router.register(r'category', CategoryViewSet,basename='category_api')
router.register(r'size', SizeViewSet,basename='size_api')
router.register(r'product', ProductViewSet,basename='product_api')
router.register(r'productproperty', ProductPropertyViewSet,basename='productproperty_api')
router.register(r'cartorder', CartOrderViewSet,basename='cartorder_api')
router.register(r'cartorderitems', CartOrderItemsViewSet,basename='cartorderitems_api')
urlpatterns =[
    path('api/',include(router.urls)),
    path('auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/users/', ListUsers.as_view()),
    path('api/register/', RegisterUser.as_view()),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
