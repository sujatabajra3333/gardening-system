from rest_framework import serializers
from main.models import *
from rest_framework.serializers import ModelSerializer
class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ['id','title','image']

class SizeSerializer(ModelSerializer):
    class Meta:
        model = Size
        fields = ['id','title']

class ProductSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = ['id','category','title','slug','detail','desc','is_featured','status']

class ProductPropertySerializer(ModelSerializer):
    class Meta:
        model = ProductProperty
        fields = ['id','product','size','price','image']

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','username', 'password']

    def create(self, validated_data):
        user = User.objects.create(username = validated_data['username'])
        user.set_password(validated_data['password'])
        user.save()
        return user

class CartOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = CartOrder
        fields = ['id', 'user','total_amt','paid_status','order_dt','order_status']

class CartOrderItemsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CartOrderItems
        fields = ['order','invoice_no','item','image','qty','price','total']
