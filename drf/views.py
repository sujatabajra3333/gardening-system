from django.shortcuts import render
from main.models import *
from rest_framework.viewsets import ModelViewSet
from .serializers import *
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication

class CategoryViewSet(ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class SizeViewSet(ModelViewSet):
    queryset = Size.objects.all()
    serializer_class = SizeSerializer

class ProductViewSet(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class ProductPropertyViewSet(ModelViewSet):
    queryset = ProductProperty.objects.all()
    serializer_class = ProductPropertySerializer

class CartOrderViewSet(ModelViewSet):
    queryset = CartOrder.objects.all()
    serializer_class = CartOrderSerializer

class CartOrderItemsViewSet(ModelViewSet):
    queryset = CartOrderItems.objects.all()
    serializer_class = CartOrderItemsSerializer

class ListUsers(APIView):
    authentication_classes = [TokenAuthentication]
    
    def get(self, request, format=None):
        usernames = [user.username for user in User.objects.all()]
        return Response(usernames)

class RegisterUser(APIView):
    def post(self, request):
        serializer = UserSerializer(data = request.data)
        if not serializer.is_valid():
            print(serializer.errors)
            return Response({'status':403 ,'errors': serializer.errors, 'message': 'Something went wrong'})
        serializer.save()

        user = User.objects.get(username = serializer.data['username'])
        token_obj  , _ = Token.objects.get_or_create(user=user)
        return Response({'status':200 ,'payload': serializer.data, 'token': str(token_obj), 'message': 'Success'})

